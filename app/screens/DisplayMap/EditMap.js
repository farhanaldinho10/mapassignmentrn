import React from 'react';
import {Image, StyleSheet, Text, TextInput, TouchableOpacity, View, Alert} from 'react-native';
import styles from './styles';
import {editMapApi} from '../../actions/asyncActions';

export default class EditMap extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.screenState.editableItem.name,
      details: this.props.screenState.editableItem.details,
      lat: this.props.screenState.editableItem.lat,
      lng: this.props.screenState.editableItem.lng,
    };
  }

  onChangeText = (field, text) => {
    this.setState({[field]: text});
  };

  submit = (id) => {
    if (this.state.name === '' || this.state.details === '' || this.state.lat === '' || this.state.lng === '') {
      Alert.alert('Error', 'Please fill all fields.');
    }
    else if (parseFloat(this.state.lng) < 5.98865807458 || parseFloat(this.state.lng) > 15.0169958839) {
      Alert.alert('Error', 'Please enter a valid longitude value.');
    }
    else if (parseFloat(this.state.lat) < 47.3024876979 || parseFloat(this.state.lat) > 54.983104153) {
      Alert.alert('Error', 'Please enter a valid latitude value.');
    }
    else {
      let body = {
        name: this.state.name,
        details: this.state.details,
        lat: parseFloat(this.state.lat),
        lng: parseFloat(this.state.lng),
      };
      editMapApi(id, body, 'Rxw5JRchFcwLEfJTMN55mpQ5Hkx5IN08umJoNBAW38uOCmmgzC3bCE55qTOsKEiX').then(response => {
        this.props.closeModal();
      }).catch(error => {
        Alert.alert('Error', error);
        console.log('Error', error);
      });

    }

  };

  render() {
    let {screenState} = this.props;

    return (
      <View style={{flexDirection: 'column', backgroundColor: '#fff', height: 350, width: 370, padding: 10}}>
        <TextInput
          onChangeText={(text) => this.onChangeText('name', text)}
          style={styles.title}
          value={this.state.name ? this.state.name : ''}
        />
        <View style={styles.spaceBetween}/>
        <TextInput
          onChangeText={(text) => this.onChangeText('details', text)}
          style={styles.field}
          value={this.state.details ? this.state.details : ''}
        />
        <View style={styles.spaceBetween}/>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text
            style={styles.field}
          >Latitude: </Text>
          <TextInput
            keyboardType="numeric"
            onChangeText={(text) => this.onChangeText('lat', text)}
            style={styles.field}
            value={`${this.state.lat ? this.state.lat : ''}`}
          />
        </View>
        <View style={styles.spaceBetween}/>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text style={styles.field}>Longitude: </Text>
          <TextInput
            keyboardType="numeric"
            style={styles.field}
            value={`${this.state.lng ? this.state.lng : ''}`}
            onChangeText={(text) => this.onChangeText('lng', text)}
          />
        </View>
        <View style={styles.spaceBetween}/>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity style={styles.button} onPress={() => this.submit(screenState.editableItem.id)}>
            <Text style={styles.buttonText}>
              Submit
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
