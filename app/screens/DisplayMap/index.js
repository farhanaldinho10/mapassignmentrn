import React, {Component} from 'react';
import Design from './DisplayMap';
import {getMapListApi, deleteMapApi, addMapApi} from '../../actions/asyncActions';
import RNGooglePlaces from 'react-native-google-places';
import Geocoder from 'react-native-geocoder';
import {Alert} from 'react-native';

export default class DisplayMapIndex extends Component {
  constructor(props) {
    super(props);
    // Geocoder.fallbackToGoogle('');      //ADD API KEY HERE LIKE  Geocoder.fallbackToGoogle('iasdnjna=1ubjsh');
    this.state = {
      data: [],
      editDialog: false,
      editableItem: null,
    };
  }


  componentDidMount() {
    this.getMaps();
  }

  getMaps = () => {
    getMapListApi('Rxw5JRchFcwLEfJTMN55mpQ5Hkx5IN08umJoNBAW38uOCmmgzC3bCE55qTOsKEiX').then(response => {
      console.log('response', response);
      this.setState({data: response});
    }).catch(error => {
      Alert.alert('Error', error);
      console.log('Error', error);
    });
  };

  addMap = () => {
    RNGooglePlaces.openAutocompleteModal({}, this.placeFields)
      .then((place) => {
        this.handlePlaceData(place);
      })
      .catch(error => console.log(error.message));  // error is a Javascript Error object
  };

  handlePlaceData(place) {
    console.log('handlePlaceData', JSON.stringify(place));
    this.place = {};
    this.place.address = place.address;
    this.place.lat = place.location.latitude;
    this.place.lng = place.location.longitude;
    this.setState({address: place.address, markerLatLng: place.location});
    this.animateCamera(place.location);
    this.getGeoCodedLocation(place.location);
  }

  getGeoCodedLocation = async (location) => {
    let ret = await Geocoder.geocodePosition({lat: location.latitude, lng: location.longitude});
    let zipCode = '', city = '', state = '', countryCode = '';
    ret.forEach(location => {
      if (zipCode.length === 0 && location.postalCode) {
        zipCode = location.postalCode;
      }

      if (city.length === 0 && location.locality) {
        city = location.locality;
      }

      if (state.length === 0 && location.adminArea) {
        state = location.adminArea;
      }

      if (countryCode.length === 0 && location.countryCode) {
        countryCode = location.countryCode;
      }
    });

    console.log('country code', countryCode);
    if (countryCode === 'DE') {
      let body = {
        name: city,
        details: city,
        lat: location.latitude,
        lng: location.longitude,
      };
      addMapApi(body, 'Rxw5JRchFcwLEfJTMN55mpQ5Hkx5IN08umJoNBAW38uOCmmgzC3bCE55qTOsKEiX').then(response => {
        console.log('response', response);
        this.getMaps();
      }).catch(error => {
        Alert.alert('Error', error);
        console.log('Error', error);
      });
    }
    else {
      Alert.alert('Error', 'Please add an address within Germany');
    }

  };

  editMap = (item) => {
    this.setState({editDialog: true, editableItem: item});
  };

  deleteMap = (item, index) => {
    let remainingData = [...this.state.data];
    remainingData.splice(index, 1);
    this.setState({data: remainingData}, () => {
      deleteMapApi(item.id, 'Rxw5JRchFcwLEfJTMN55mpQ5Hkx5IN08umJoNBAW38uOCmmgzC3bCE55qTOsKEiX').then(response => {
        this.getMaps();
      }).catch(error => {
        Alert.alert('Error', error);
        console.log('Error', error);
      });
    });

  };

  onMapReady = () => {
    this.animateCamera(false);
  };

  animateCamera = async (location) => {
    const camera = await this.map.getCamera();
    camera.zoom = 6;
    if (location) {
      camera.center.latitude = location.latitude;
      camera.center.longitude = location.longitude;
    }
    this.map.animateCamera(camera, {duration: 2000});
  };

  closeModal = () => {
    this.setState({editDialog: false}, () => {
      this.getMaps();
    });
  };


  render() {
    return (
      <Design
        screenState={this.state}
        addMap={this.addMap}
        editMap={this.editMap}
        deleteMap={this.deleteMap}
        setMapRef={ref => this.map = ref}
        onMapReady={this.onMapReady}
        closeModal={this.closeModal}
      />
    );
  }
}
