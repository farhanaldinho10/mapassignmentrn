import {StyleSheet} from 'react-native';


export default styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#fff'},
  field: {
    fontSize: 17,
    fontFamily: 'Lato-Bold',
    color: '#555555',
  },
  editDelete: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#555555',
    width: 90,
    height: 40,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  spaceBetween: {marginTop: 5},
  title: {
    fontSize: 30,
    fontFamily: 'Lato-Bold',
    color: '#5475ed',
  },
  borderLine: {marginTop: 20, marginBottom: 20, borderWidth: 1, borderColor: '#d2d2d2'},
  button: {
    backgroundColor: '#5475ed',
    width: 150,
    height: 40,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontFamily: 'Lato-Bold',
    color: '#ffffff',
  },
});
