import React, {Component} from 'react';
import {View, Text, TouchableOpacity, FlatList, ScrollView} from 'react-native';
import styles from './styles';
import Modal from 'react-native-modal';
import MapView, {Marker} from 'react-native-maps';
import EditMap from './EditMap';

export default class DisplayMap extends Component {

  renderItem = ({item, index}) => {
    let {editMap, deleteMap} = this.props;
    return (
      <View style={{flexDirection: 'column'}}>
        <View style={styles.borderLine}/>
        <Text
          style={styles.title}
        >{item.name}</Text>
        <View style={styles.spaceBetween}/>
        <Text
          style={styles.field}
        >{item.details}</Text>
        <View style={styles.spaceBetween}/>
        <View style={{flexDirection: 'row'}}>
          <Text
            style={styles.field}
          >Latitude: </Text>
          <Text
            style={styles.field}
          >{item.lat}</Text>
        </View>
        <View style={styles.spaceBetween}/>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.field}>Longitude: </Text>
          <Text
            style={styles.field}
          >{item.lng}</Text>
        </View>
        <View style={styles.spaceBetween}/>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <TouchableOpacity style={styles.editDelete} onPress={() => editMap(item)}>
            <Text style={styles.field}>Edit</Text>
          </TouchableOpacity>
          <Text style={[styles.field, {marginStart: 10, marginEnd: 10}]}>or</Text>
          <TouchableOpacity
            style={styles.editDelete}
            onPress={() => deleteMap(item, index)}>
            <Text style={styles.field}>Delete</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };


  render() {
    let {addMap, screenState, onMapReady, setMapRef, closeModal} = this.props;
    return (
      <ScrollView style={styles.container}>
        <View style={{padding: 20}}>
          <MapView
            onMapReady={onMapReady}
            ref={setMapRef}
            style={{height: 250, width: 280}}
            initialRegion={{
              latitude: 52.520008,
              longitude: 13.404954,
              latitudeDelta: 0.0922,
              longitudeDelta: 0.0421,
            }}>
            {screenState.data && screenState.data.map(marker => (
              <Marker key={marker.name}
                      coordinate={{latitude: marker.lat, longitude: marker.lng}}
                      title={marker.name}
              />
            ))}
          </MapView>
          <View style={{marginTop: 20}}>
            <TouchableOpacity style={styles.button} onPress={addMap}>
              <Text style={styles.buttonText}>
                Add Map
              </Text>
            </TouchableOpacity>
          </View>
          {screenState.data && <FlatList
            data={screenState.data}
            renderItem={this.renderItem}
            keyExtractor={(item, index) => index + ''}/>}
        </View>
        <Modal onBackdropPress={() => {
          closeModal();
        }} isVisible={screenState.editDialog}>
          <EditMap
            screenState={screenState}
            closeModal={closeModal}/>
        </Modal>
      </ScrollView>
    );
  }
}
