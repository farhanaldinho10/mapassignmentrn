import {
  apiFetch,
  apiFetchPost,
  apiFetchPatch,
  apiFetchDelete,
} from '../utils/network';

export const addMapApi = (body, accessToken) => {
  return apiFetchPost(`api/mapDetails?access_token=${accessToken}`, body, accessToken)
    .then(data => {
      return data;
    })
    .catch(err => {
      alert(err);
    });
};

export const editMapApi = (id, body, accessToken) => {
  return apiFetchPatch(`api/mapDetails/${id}?access_token=${accessToken}`, body, accessToken)
    .then(data => {
      return data;
    })
    .catch(err => {
      alert(err);
    });
};

export const deleteMapApi = (id, accessToken) => {
  return apiFetchDelete(`api/mapDetails/${id}?access_token=${accessToken}`, accessToken)
    .then(data => {
      return data;
    })
    .catch(err => {
      alert(err);
    });
};

export const getMapListApi = (accessToken) => {
  return apiFetch(`api/mapDetails/?access_token=${accessToken}`, accessToken)
    .then(data => {
      return data;
    })
    .catch(err => {
      alert(err);
    });
};
