import {Alert} from 'react-native';
import {constants} from '../config/constants';

const extractResult = async res => {
  console.log('Not 200', res);
  if (res.status === 204) {
    return Promise.resolve('');
  }
  if (res.status !== 200) {
    console.log('Not 200', res);
  }
  const resp = await res.json();
  console.log('REQUEST RESPONSE', resp);
  if (resp && resp.error) {
    console.log('Has error', resp.error);
    if (resp.error.message === 'Authorization Required') {
      console.log('Authorization Check');
    }
    else {
      return Promise.reject(resp.error.message);
    }
  }
  return Promise.resolve(resp);
};

const extractError = err => {
  return Promise.reject(err);
};

const getAuthHeadersObj = token => {
  return token ?
    {
      headers: {
        Authorization: 'Bearer ' + token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    } :
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };
};

export const apiFetch = (url, token) => {
  const requestUrl = constants.kBaseUrl + url;
  console.log('Request url:', requestUrl);
  return fetch(requestUrl, getAuthHeadersObj(token))
    .then(extractResult)
    .catch(extractError);
};


export const apiFetchPost = (url, body, token) => {
  let requestData = {
    method: 'POST',
    body: JSON.stringify(body),
  };
  requestData.headers = getAuthHeadersObj(token).headers;
  const requestUrl = constants.kBaseUrl + url;
  console.log('Request url:', requestUrl);
  console.log('Request Body', requestData);
  return fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError);
};

export const apiFetchPatch = (url, body, token) => {
  let requestData = {
    method: 'PATCH',
    body: JSON.stringify(body),
  };
  requestData.headers = getAuthHeadersObj(token).headers;
  const requestUrl = constants.kBaseUrl + url;
  console.log('Request url:', requestUrl);
  console.log('Request Body', requestData);
  return fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError);
};

export const apiFetchDelete = (url, token) => {
  let requestData = {
    method: 'DELETE',
  };
  requestData.headers = getAuthHeadersObj(token).headers;
  const requestUrl = constants.kBaseUrl + url;
  console.log('Request url:', requestUrl);
  console.log('Request Body', requestData);
  return fetch(requestUrl, requestData)
    .then(extractResult)
    .catch(extractError);
};
